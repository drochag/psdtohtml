module.exports = {
  siteMetadata: {
    title: 'PSD to HTML Excercise',
  },
  plugins: [`gatsby-plugin-sass`, `gatsby-plugin-react-helmet`],
};
