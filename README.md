# PSD TO HTML Excercise

## Intro

I'm not very used to *not* use version management, so I used bitbucket, this same project is hosted on https://bitbucket.org/DanMMX/psdtohtml. Zipped it without the node_modules folder so it's not that heavy.

*Note:* I don't own / pay Adobe, usually documents are provided to me annotated and I translate them to a website. I can extract annotations, though that will be extra to pay for Adobe. I used Sketch (which I already own) and Sip for color picks, so if it's not exact that's why - also the fonts and font sizes. No extra for using Sketch files.

## Timing

Might seem very big, but a dog got accidented near my home and assisted. Also took a little bit longer because I considered mobile version.

## Prod version

You can see this project live on http://psdtohtml.surge.sh

## Installing

```
npm install -g gatsby
npm install
npm run develop
```

## Production build

```
npm run build
```

## Deploy

```
npm run deploy
```