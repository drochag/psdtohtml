import React from 'react'
import Link from 'gatsby-link'
import fontawesome from '@fortawesome/fontawesome';
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import brands from '@fortawesome/fontawesome-free-brands'

fontawesome.library.add(brands);

import '../styles/main.scss'

const IndexPage = () => (
  <div className="Contact">
    <div className="Contact-breadcrumbContainer">
      <ol className="Contact-breadcrumb">
        <li className="Contact-breadcrumb-item">Home</li>
        <li className="Contact-breadcrumb-item">Who we are</li>
        <li className="Contact-breadcrumb-item active">Contact</li>
      </ol>
    </div>
    <div className="Contact-title">Contact</div>
    <div className="Contact-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim</div>
    <div className="Contact-boxes">
      <div className="Contact-box">
        <div className="Contact-boxTitle">Contact us</div>
        <form className="Contact-form">
          <input className="Contact-input" type="text" placeholder="Name *" />
          <input className="Contact-input Contact-input--half" type="text" placeholder="Phone *" />
          <input className="Contact-input Contact-input--half Contact-input--halfNoMargin" type="text" placeholder="Email *" />
          <textarea className="Contact-input Contact-input--textarea" placeholder="Message *" />
          <button className="Contact-button">Submit</button>
        </form>
      </div>
      <div className="Contact-box Contact-box--right">
        <div className="Contact-boxTitle">Reach us</div>
        <div className="Contact-info">
          Coalition Skills Test<br />
          535 La Plata Street<br />
          4200 Argentina<br /><br />
          Phone: 385.154.11.28.38<br />
          Fax: 385.154.36.66.78
        </div>
        <div className="Contact-socials">
          <div className="Contact-socialContainer"><FontAwesomeIcon icon={["fab", "facebook-f"]} size="lg" color="white" className="Contact-social Contact-social--active" /></div>
          <div className="Contact-socialContainer"><FontAwesomeIcon icon={["fab", "twitter"]} size="lg" color="white" className="Contact-social" /></div>
          <div className="Contact-socialContainer"><FontAwesomeIcon icon={["fab", "linkedin-in"]} size="lg" color="white" className="Contact-social" /></div>
          <div className="Contact-socialContainer"><FontAwesomeIcon icon={["fab", "pinterest-p"]} size="lg" color="white" className="Contact-social" /></div>
        </div>
      </div>
    </div>
  </div>
)

export default IndexPage
