import React from 'react'
import Link from 'gatsby-link'

const Menu = () => (
  <div className="Menu">
    <div className="Menu-container">
      <div className="Menu-logoContainer">
        Your<span className="Menu-logo">Logo</span>
      </div>
      <ul className="Menu-options">
        <li className="Menu-option">Title 1</li>
        <li className="Menu-option Menu-option--submenu">Title 2
          <ul className="Menu-options">
            <li className="Menu-option">Submenu 1</li>
            <li className="Menu-option">Submenu 2</li>
            <li className="Menu-option">Submenu 3
              <ul className="Menu-options">
                <li className="Menu-option">Submenu 1</li>
                <li className="Menu-option">Submenu 2</li>
                <li className="Menu-option">Submenu 3</li>
              </ul>
            </li>
          </ul>
        </li>
        <li className="Menu-option">Title 3</li>
        <li className="Menu-option">Title 4</li>
        <li className="Menu-option">Title 5</li>
        <li className="Menu-option">Title 6</li>
        <li className="Menu-option">Title 7</li>
      </ul>
    </div>
  </div>
)

export default Menu
