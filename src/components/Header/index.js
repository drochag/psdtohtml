import React from 'react'
import Link from 'gatsby-link'

const Header = () => (
  <div className="Header">
    <div className="Header-container">
      <div className="Header-phoneContainer">
        Call us Now!<span className="Header-phone">385.154.11.28.35</span>
      </div>
      <div className="Header-actions">
        <a href="#" className="Header-action">Login</a>
        <a href="#" className="Header-action Header-action--signup">Signup</a>
      </div>
    </div>
  </div>
)

export default Header
